<?php

/**
 * @file
 * Install and uninstall hooks for the simple_sitemap_views module.
 */

/**
 * Implements hook_install().
 */
function simple_sitemap_views_install() {
  // Enable simple_sitemap_display_extender plugin.
  /** @var \Drupal\Core\Config\Config $config */
  $config = \Drupal::service('config.factory')->getEditable('views.settings');
  $display_extenders = $config->get('display_extenders') ?: [];
  $display_extenders[] = 'simple_sitemap_display_extender';
  $config->set('display_extenders', $display_extenders);
  $config->save();
}

/**
 * Implements hook_uninstall().
 */
function simple_sitemap_views_uninstall() {
  // Disable simple_sitemap_display_extender plugin.
  /** @var \Drupal\Core\Config\Config $config */
  $config = \Drupal::service('config.factory')->getEditable('views.settings');
  $display_extenders = $config->get('display_extenders') ?: [];

  $key = array_search('simple_sitemap_display_extender', $display_extenders);
  if ($key !== FALSE) {
    unset($display_extenders[$key]);
    $config->set('display_extenders', $display_extenders);
    $config->save();
  }
}

/**
 * Implements hook_schema().
 */
function simple_sitemap_views_schema() {
  $schema['simple_sitemap_views'] = [
    'description' => 'Index of argument values for view pages.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique ID for argument values.',
      ],
      'view_id' => [
        'type' => 'varchar_ascii',
        'not null' => TRUE,
        'default' => '',
        'length' => 128,
        'description' => 'The ID of the view.',
      ],
      'display_id' => [
        'type' => 'varchar_ascii',
        'not null' => TRUE,
        'default' => '',
        'length' => 128,
        'description' => 'The ID of the view display.',
      ],
      'arguments_ids' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 1024,
        'description' => 'A string representation of the set of argument identifiers.',
      ],
      'arguments_values' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 1024,
        'description' => 'A string representation of the set of argument values.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'view' => ['view_id'],
      'display' => ['view_id', 'display_id'],
      'arguments_ids' => ['view_id', 'display_id', 'arguments_ids'],
    ],
  ];
  return $schema;
}
